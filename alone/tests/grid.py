import unittest

from alone.grid import Grid


class TestGrid(unittest.TestCase):

    def _assert_grid_equal(self, grid: Grid, rows: list[list[int]]):
        assert len(rows) == grid.size
        assert all(len(row) == grid.size
                   for row in rows)

        for i, row in enumerate(rows):
            for j, cell_value in enumerate(row):
                index = i * grid.size + j
                self.assertEqual(grid.data[index], cell_value,
                                 f'Different cell values at ({i}, {j})')

    def test_initial_state(self):
        """Initial value for all cells is expected to be zero"""
        grid = Grid(3)
        self._assert_grid_equal(grid, [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ])


class TestGridFloodFill(TestGrid):

    def setUp(self):
        self.grid = Grid(5)

    def test_fill_empty_grid(self):
        """Test filling up an empty grid from various starting points"""
        for start_row, start_col in ((0, 0), (0, 4), (4, 0), (4, 4), (2, 2)):
            grid = Grid(5)
            grid.flood_fill(start_row, start_col, 1)
            try:
                self._assert_grid_equal(grid, [
                    [1, 1, 1, 1, 1],
                    [1, 1, 1, 1, 1],
                    [1, 1, 1, 1, 1],
                    [1, 1, 1, 1, 1],
                    [1, 1, 1, 1, 1],
                ])
            except AssertionError:
                raise AssertionError('Failed flood fill from '
                                     f'({start_row}, {start_col})')

    def test_fill_region_surrounded_by_blackout(self):
        """Tests filling a region surrounded by BLACKED_OUT cells"""
        grid = Grid(5)
        grid.set_values([
            [ 0,  0, -1,  0,  0],
            [ 0, -1,  0, -1,  0],
            [-1,  0,  0,  0, -1],
            [ 0, -1,  0,  0, -1],
            [ 0,  0, -1, -1,  0],
        ])
        grid.flood_fill(2, 1, 3)
        self._assert_grid_equal(grid, [
            [ 0,  0, -1,  0,  0],
            [ 0, -1,  3, -1,  0],
            [-1,  3,  3,  3, -1],
            [ 0, -1,  3,  3, -1],
            [ 0,  0, -1, -1,  0],
        ])

    def test_fill_larger_region(self):
        """Tests filling a larger region in the interior of the grid"""
        grid = Grid(5)
        grid.set_values([
            [ 0,  0, -1,  0,  0],
            [ 0, -1,  0,  0,  0],
            [-1,  0,  0,  0, -1],
            [ 0,  0,  0,  0, -1],
            [ 0,  0, -1, -1,  0],
        ])
        grid.flood_fill(2, 1, 3)
        self._assert_grid_equal(grid, [
            [ 0,  0, -1,  3,  3],
            [ 0, -1,  3,  3,  3],
            [-1,  3,  3,  3, -1],
            [ 3,  3,  3,  3, -1],
            [ 3,  3, -1, -1,  0],
        ])

    def test_out_of_bounds_start_cell(self):
        with self.assertRaises(ValueError):
            self.grid.flood_fill(5, 2, 4)


if __name__ == '__main__':
    unittest.main()
