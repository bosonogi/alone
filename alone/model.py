import random
from abc import ABC, abstractmethod
from copy import deepcopy
from dataclasses import dataclass
from typing import Tuple, Optional

from alone.board import generate_board

# top left, top right, bottom left, bottom right
Corners = Tuple[str, str, str, str]


@dataclass
class Position:

    row: int
    col: int

    def __eq__(self, other: 'Position') -> bool:
        return self.row == other.row and self.col == other.col


@dataclass
class Cell:

    number: int
    position: Position
    marked: bool = False
    valid: bool = True
    group: Optional[int] = None

    def sync(self, other: 'Cell') -> bool:
        assert self.position == other.position
        assert self.number == other.number

        updated = False

        if self.marked != other.marked:
            self.marked = other.marked
            updated = True

        if self.valid != other.valid:
            self.valid = other.valid
            updated = True

        return updated


class Outline(ABC):

    @property
    @abstractmethod
    def line_h(self):
        pass

    @property
    @abstractmethod
    def line_v(self):
        pass

    @property
    @abstractmethod
    def _corners(self):
        pass

    def __init__(self):
        self.top_left = self._get_corner_chars(0)
        self.top_right = self._get_corner_chars(1)
        self.bottom_left = self._get_corner_chars(2)
        self.bottom_right = self._get_corner_chars(3)
        self.top = self._get_corner_chars(4)
        self.bottom = self._get_corner_chars(5)
        self.left = self._get_corner_chars(6)
        self.right = self._get_corner_chars(7)
        self.middle = self._get_corner_chars(8)

    def _get_corner_chars(self, index) -> Corners:
        """Get corners for a particular cell position"""
        tl, tr = self._corners[0][index]
        bl, br = self._corners[1][index]
        return tl, tr, bl, br

    def corners(self, pos: Position, game: 'Game') -> Corners:
        """Determine corner characters for a cell"""
        match pos.row, pos.col:
            case (0, 0):
                return self.top_left
            case (0, game.max_index):
                return self.top_right
            case (game.max_index, 0):
                return self.bottom_left
            case (game.max_index, game.max_index):
                return self.bottom_right
            case (0, _):
                return self.top
            case (game.max_index, _):
                return self.bottom
            case (_, 0):
                return self.left
            case (_, game.max_index):
                return self.right
        return self.middle


class Game:

    def __init__(self, grid_size: int, cell: Outline, cursor: Outline):
        seed = random.randint(1000000, 9999999)
        print('seed:', seed)
        data = generate_board(grid_size, 0.3, seed)

        self.grid_size = grid_size
        self.max_index = grid_size - 1
        self.cursor = Position(row=0, col=0)
        self.board = [[Cell(number=data.grid.get(row, col),
                            position=Position(row, col))
                       for col in range(grid_size)]
                       for row in range(grid_size)]

        self.virtual_board = deepcopy(self.board)

        self.cell_outline = cell
        self.cursor_outline = cursor

    @property
    def current_cell(self) -> Cell:
        return self.board[self.cursor.row][self.cursor.col]

    def all_cells(self):
        for row in self.board:
            for cell in row:
                yield cell

    def all_marked_cells(self):
        for cell in self.all_cells():
            if cell.marked:
                yield cell

    def get_cell(self, position: Position):
        return self.board[position.row][position.col]

    def neighbors(self, cell: Cell):
        for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
            pos = Position(cell.position.row + dx,
                           cell.position.col + dy)
            if self.grid_size > pos.row >= 0 and self.grid_size > pos.col >= 0:
                yield self.get_cell(pos)
