import sys
from enum import Enum
from typing import Any

from . import model


class C(Enum):

    """ANSI codes that are used"""

    home = 'H'
    cross = '9m'
    uncross = '29m'
    gray = '90m'
    blue = '34m'
    green = '32m'
    red = '31m'
    default = '39m'
    up = '{}A'
    down = '{}B'
    right = '{}C'
    left = '{}D'


def p(*strings):
    for s in strings:
        sys.stdout.write(s)


def a(code: C) -> str:
    return '\033[{}'.format(code.value)


def aa(code: C, arg: Any) -> str:
    return '\033[{}'.format(code.value.format(arg))


class CellOutline(model.Outline):

    line_h = '─'
    line_v = '│'

    _corners = ('┌┬ ┬┐ ├┼ ┼┤ ┬┬ ┼┼ ├┼ ┼┤ ┼┼'.split(),
                '├┼ ┼┤ └┴ ┴┘ ┼┼ ┴┴ ├┼ ┼┤ ┼┼'.split())


class CursorOutline(model.Outline):

    line_h = '━'
    line_v = '┃'

    _corners = ('┏┱ ┲┓ ┢╅ ╆┪ ┲┱ ╆╅ ┢╅ ╆┪ ╆╅'.split(),
                '┡╃ ╄┩ ┗┹ ┺┛ ╄╃ ┺┹ ┡╃ ╄┩ ╄╃'.split())


def format_value(cell: model.Cell) -> str:
    if cell.marked:
        prefix = a(C.cross)
        postfix = a(C.uncross)
    else:
        prefix = postfix = ''
    return f'{prefix} {cell.number} {postfix}'


def draw_cell_outline(pos: model.Position, outline: model.Outline,
                      game: model.Game, color: C):
    p(a(C.home), a(color))
    if pos.row > 0:
        p(aa(C.down, 2 * pos.row))
    if pos.col > 0:
        p(aa(C.right, 4 * pos.col))

    tl, tr, bl, br = outline.corners(pos, game)

    p(tl, 3 * outline.line_h, tr)
    p(aa(C.left, 5), aa(C.down, 1))
    p(outline.line_v, aa(C.right, 3), outline.line_v)
    p(aa(C.left, 5), aa(C.down, 1))
    p(bl, 3 * outline.line_h, br)


def draw_cell_value(cell: model.Cell, valid_color=C.default):
    color = valid_color if cell.valid else C.red
    p(a(C.home), a(color))

    if cell.position.row > 0:
        p(aa(C.down, 2 * cell.position.row))
    if cell.position.col > 0:
        p(aa(C.right, 4 * cell.position.col))

    p(aa(C.right, 1), aa(C.down, 1), format_value(cell))
