import array

UNINITIALIZED = 0
BLACKED_OUT = -1
MAX_SIZE = 9

# This stack is shared between all :class:`Grid` instances
_stack = array.array('B', [0] * MAX_SIZE**2)


class Grid:

    __slots__ = ('size', 'data')

    def __init__(self, size: int, values=None):
        assert size <= MAX_SIZE
        if values is not None:
            assert len(values) == size**2
        else:
            values = [UNINITIALIZED] * size**2
        self.size = size
        self.data = array.array('b', values)

    def clone(self):
        return type(self)(self.size, self.data)

    def set_values(self, rows: list[list[int]]):
        assert len(rows) == self.size
        assert all(len(row) == self.size
                   for row in rows)

        for i, row in enumerate(rows):
            for j, cell_value in enumerate(row):
                index = i * self.size + j
                self.data[index] = cell_value

    def print(self):
        for i in range(self.size):
            for j in range(self.size):
                index = i * self.size + j
                print(str(self.data[index]).rjust(2), end=' ')
            print()
        print()

    def get(self, row: int, col: int) -> int:
        return self.data[row * self.size + col]

    def set(self, row: int, col: int, value: int):
        self.data[row * self.size + col] = value

    def row_numbers(self, row):
        for col in range(self.size):
            if (cell := self.get(row, col)) != UNINITIALIZED:
                yield cell

    def col_numbers(self, col):
        for row in range(self.size):
            if (cell := self.get(row, col)) != UNINITIALIZED:
                yield cell

    def flood_fill(self, start_row: int, start_col: int, fill_value: int):
        """Perform a flood fill starting from the specified position

        :param start_row: The row index of the starting cell.
        :param start_col: The column index of the starting cell.
        :param fill_value: The value to use for filling.
        :raises ValueError: If the starting position is out of bounds,
            or if the fill value is not positive.
        """
        if not (0 <= start_row < self.size and 0 <= start_col < self.size):
            raise ValueError('Start cell out of bounds')

        if not fill_value > 0:
            raise ValueError('Fill value must be positive')

        stack_top = 1
        _stack[0] = start_row * self.size + start_col

        while stack_top > 0:
            stack_top -= 1
            index = _stack[stack_top]

            cell_value = self.data[index]
            if cell_value == BLACKED_OUT or cell_value != UNINITIALIZED:
                continue

            self.data[index] = fill_value

            # Push neighbours to stack
            row, col = divmod(index, self.size)
            for dr, dc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                new_row, new_col = row + dr, col + dc
                if 0 <= new_row < self.size and 0 <= new_col < self.size:
                    new_index = new_row * self.size + new_col
                    for stack_index in range(stack_top):
                        if _stack[stack_index] == new_index:
                            break  # index already on the stack
                    else:
                        _stack[stack_top] = new_index
                        stack_top += 1
