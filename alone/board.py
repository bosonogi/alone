import random
from itertools import chain

import alone
from alone.grid import Grid, BLACKED_OUT, UNINITIALIZED


class Board:
    def __init__(self, grid: Grid, meta_grid: Grid):
        self.grid = grid
        self.meta_grid = meta_grid


def _connected(grid: Grid) -> bool:
    """Check if all cells that are not blacked are connected

    Flood-filling from an uninitialised cell should initialize all
    uninitialised cells.

    Note: this modifies the grid.
    """
    # Find the first uninitialized cell
    for index, value in enumerate(grid.data):
        if value == UNINITIALIZED:
            break
    else:
        raise RuntimeError('All cells are initialized, cannot flood fill')

    # Flood-fill from there
    row, col = divmod(index, grid.size)
    grid.flood_fill(row, col, 1)

    # If any cell is left uninitialized, the grid is not connected
    for value in grid.data:
        if value == UNINITIALIZED:
            return False

    return True


def _black_out_cells(grid: Grid, cells_to_black_out: int):
    """Blacks out the given number of cells in the grid

    An uninitialized grid is expected.
    Cells are blacked out in a way that leaves the remaining cells connected.
    A BFS approach is used, without an exhaustive search (to be quicker).

    This means that the grid might end up with less blacked out cells than
    asked for by ``cells_to_black_out``.
    """
    # All grid cells are candidates for blacking out to start with
    candidate_indices = list(range(grid.size**2))
    random.shuffle(candidate_indices)

    count = 0
    while len(candidate_indices) > 0 and count < cells_to_black_out:
        index = candidate_indices.pop()

        grid_clone = grid.clone()
        grid_clone.data[index] = BLACKED_OUT
        if not _connected(grid_clone):
            continue  # Blacking out this index makes the board invalid

        grid.data[index] = BLACKED_OUT
        count += 1

        # Remove adjacent cells from possible cells to mark
        row, col = divmod(index, grid.size)
        for dr, dc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            new_row, new_col = row + dr, col + dc
            if 0 <= new_row < grid.size and 0 <= new_col < grid.size:
                forbidden_index = new_row * grid.size + new_col
                try:
                    candidate_indices.remove(forbidden_index)
                except ValueError:
                    pass  # Already removed


def _value_candidates(grid: Grid, row: int, col: int) -> list[int]:
    """Candidates for a value that is not a duplicate"""
    candidates = list(range(1, grid.size + 1))
    for value in chain(grid.col_numbers(col),
                       grid.row_numbers(row)):
        try:
            candidates.remove(value)
        except ValueError:
            pass  # Already removed
    random.shuffle(candidates)
    return candidates


def _init_board_numbers(meta_grid: Grid):
    """Create and initialize a new board based on given blacked out cells"""
    # Cells that need to be populated with a valid solution
    empty_cells = [(row, col)
                   for row in range(meta_grid.size)
                   for col in range(meta_grid.size)
                   if meta_grid.get(row, col) != BLACKED_OUT]


    def fill_in(current_grid: Grid, i: int):
        """Search for a combination of cell values that is a valid solution

        A DFS approach is used.
        """
        row, col = empty_cells[i]
        candidates = _value_candidates(current_grid, row, col)
        if len(candidates) == 0:
            raise ValueError('No candidates found')

        for candidate in candidates:
            next_grid = current_grid.clone()
            next_grid.set(row, col, candidate)
            try:
                if i == len(empty_cells) - 1:
                    return next_grid
                else:
                    return fill_in(next_grid, i + 1)
            except ValueError:
                continue

        raise ValueError('Dead end')

    # Fill in cells that are not blacked out
    # Do not repeat numbers in rows and columns
    filled_in_grid = fill_in(Grid(meta_grid.size), 0)

    # Fill in cells that are blacked out with numbers that are already
    # in that row or column
    for row in range(meta_grid.size):
        for col in range(meta_grid.size):
            if meta_grid.get(row, col) == BLACKED_OUT:
                numbers = [value
                           for value in chain(filled_in_grid.col_numbers(col),
                                              filled_in_grid.row_numbers(row))]
                filled_in_grid.set(row, col, random.choice(numbers))

    return filled_in_grid


def generate_board(size: int, blacked_out: float, seed: int) -> Board:
    """Generate a solvable Hitori board

    :param size: Size of the board (integer between 4 and 9)
    :param blacked_out: Ratio of blacked out cells on the Hitori board.
    :param seed: Random number generator seed.
    """
    if not (alone.MIN_BOARD_SIZE <= size <= alone.MAX_BOARD_SIZE):
        raise ValueError('"size" must be an int between {} and {}'.format(
            alone.MIN_BOARD_SIZE, alone.MAX_BOARD_SIZE
        ))
    if not (alone.MIN_BLACKED_OUT <= blacked_out <= alone.MAX_BLACKED_OUT):
        raise ValueError('"blacked_out" must be between {} and {}'.format(
            alone.MIN_BLACKED_OUT, alone.MAX_BLACKED_OUT
        ))

    random.seed(seed)

    meta_grid = Grid(size)
    _black_out_cells(meta_grid, int(round(blacked_out * size ** 2)))

    grid = _init_board_numbers(meta_grid)
    return Board(grid, meta_grid)


def test():
    for _ in range(10000):
        seed = random.randint(1000000, 9999999)
        print(seed)
        board = generate_board(9, 0.3, seed)
        board.grid.print()

        for row in range(board.grid.size):
            for col in range(board.grid.size):
                if board.grid.get(row, col) == UNINITIALIZED:
                    raise RuntimeError('Board is uninitialized')


if __name__ == '__main__':
    test()
