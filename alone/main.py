import curses
import random
import sys
import time
from itertools import count
from typing import Tuple

from . import model, rules, ui, MIN_BOARD_SIZE, MAX_BOARD_SIZE
from .ui import C

game: model.Game


def draw(delta_row, delta_col, redraw_cell):
    if delta_row or delta_col:
        ui.draw_cell_outline(game.cursor, game.cell_outline, game, C.gray)

        game.cursor.row += delta_row
        game.cursor.col += delta_col

        ui.draw_cell_outline(game.cursor, game.cursor_outline, game, C.default)

    elif redraw_cell:
        rules.reset_validations(game)
        rules.no_adjacent_marks(game)
        rules.cell_groups(game)

        for cell in game.all_cells():
            virtual = game.virtual_board[cell.position.row][cell.position.col]
            if virtual.sync(cell):
                ui.draw_cell_value(cell)

        if all(cell.valid for cell in game.all_cells()) and rules.solved(game):
            raise StopIteration('Game won')

    sys.stdout.flush()


def update(input_key) -> Tuple[int, int, bool]:
    redraw_cell = False
    delta_row = delta_col = 0

    match input_key:
        case curses.KEY_LEFT:
            if game.cursor.col > 0:
                delta_col = -1
        case curses.KEY_RIGHT:
            if game.cursor.col < game.max_index:
                delta_col = 1
        case curses.KEY_UP:
            if game.cursor.row > 0:
                delta_row = -1
        case curses.KEY_DOWN:
            if game.cursor.row < game.max_index:
                delta_row = 1
        case 32:  # space
            cell = game.current_cell
            cell.marked = not cell.marked
            redraw_cell = True

    return delta_row, delta_col, redraw_cell


def game_over(window):
    def diagonals(n):
        yield model.Position(n, n)

        for dn in count(1):
            more = False
            for direction in (-1, 1):
                diff = dn * direction
                row, col = n - diff, n + diff
                if game.grid_size > row >= 0 and game.grid_size > col >= 0:
                    yield model.Position(row, col)
                    more = True
            if not more:
                break

    all_cells1 = list(game.all_cells())
    all_cells2 = [game.get_cell(pos)
                  for x in range(game.grid_size)
                  for pos in diagonals(x)]

    for cell in all_cells1:
        if cell not in all_cells2:
            all_cells2.append(cell)

    random.shuffle(all_cells1)
    for cell1, cell2, in zip(all_cells1, all_cells2):
        ui.draw_cell_outline(cell1.position, game.cell_outline, game, C.gray)
        ui.draw_cell_value(
            cell2, valid_color=C.blue if cell2.marked else C.green
        )

        # Sync to window
        sys.stdout.flush()
        window.noutrefresh()

        time.sleep(0.1)

    window.getch()


def play(window: curses.window):
    # Hide the cursor
    curses.curs_set(0)

    # Draw the board
    for cell in game.all_cells():
        ui.draw_cell_outline(cell.position, game.cell_outline, game, C.gray)
        ui.draw_cell_value(cell)

    # Draw the cursor
    ui.draw_cell_outline(game.cursor, game.cursor_outline, game, C.default)

    # Sync to window
    sys.stdout.flush()
    window.noutrefresh()

    # Game loop
    while (c := window.getch()) != curses.KEY_BACKSPACE:
        try:
            draw(*update(c))
        except StopIteration:
            game_over(window)
            break


def run():
    global game
    try:
        board_size = int(sys.argv[1])
        if not (MIN_BOARD_SIZE <= board_size <= MAX_BOARD_SIZE):
            raise ValueError()
    except IndexError:
        print('Specify board size as an argument')
    except ValueError:
        print('Board size needs to be an integer between {} and {}'.format(
            MIN_BOARD_SIZE, MAX_BOARD_SIZE
        ))
    else:
        game = model.Game(board_size, ui.CellOutline(), ui.CursorOutline())
        curses.wrapper(play)
