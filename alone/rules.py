from collections import Counter

from .model import Cell, Game


def reset_validations(game: Game):
    for cell in game.all_cells():
        cell.valid = True
        cell.group = None


def no_adjacent_marks(game: Game):
    for cell in game.all_marked_cells():
        for other_cell in game.neighbors(cell):
            if other_cell.marked:
                other_cell.valid = False
                cell.valid = False


def _visit(cell: Cell, game: Game, group_id):
    cell.group = group_id
    for neighbor in game.neighbors(cell):
        if not neighbor.marked and neighbor.group is None:
            _visit(neighbor, game, group_id)


def cell_groups(game: Game):
    # Identify groups of unmarked cells
    group_id = 0
    while True:
        # Find an unmarked and ungrouped cell
        for cell in game.all_cells():
            if not cell.marked and cell.group is None:
                # Flood fill from starting_cell
                _visit(cell, game, group_id)
                break
        else:
            break  # No such cells

        group_id += 1

    # Group sizes
    stats = Counter(cell.group for cell in game.all_cells() if not cell.marked)

    # If more than one group mark smaller groups as not valid
    if len(stats) > 1:
        most_common_group_id, _ = stats.most_common(1)[0]

        for cell in game.all_cells():
            if not cell.marked and cell.group != most_common_group_id:
                cell.valid = False
                for neighbor in game.neighbors(cell):
                    if neighbor.marked:
                        neighbor.valid = False


def solved(game: Game) -> bool:
    for row in game.board:
        unmarked = [cell.number for cell in row if not cell.marked]
        if len(unmarked) != len(set(unmarked)):
            return False

    for col in range(game.grid_size):
        unmarked = [cell.number
                    for row in game.board
                    if not (cell := row[col]).marked]
        if len(unmarked) != len(set(unmarked)):
            return False

    return True
